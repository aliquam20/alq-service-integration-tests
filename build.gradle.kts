import org.aliquam.AlqUtils
import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
	id("java")
	id("org.sonarqube") version "3.3"
	jacoco
	id("io.freefair.lombok") version "6.2.0"
	id("org.aliquam.alq-gradle-parent") version "0.4.14"
}
val alq = AlqUtils(project).withStandardProjectSetup()

group = "org.aliquam"
val artifactId = "alq-service-integration-tests"
val baseVersion = "0.0.1"
version = alq.getSemVersion(baseVersion)

val branchName: String? = System.getenv("BRANCH_NAME")
val isJenkins = branchName != null
val dockerRegistryHost = if (isJenkins) alq.getEnvOrPropertyOrThrow("DOCKER_REGISTRY_HOST") else null
val dockerImageName = "aliquam/${artifactId}"

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
	implementation("org.aliquam:alq-cum:0.0.1-DEV_BUILD")
	implementation("org.aliquam:alq-config-api:0.0.1-DEV_BUILD")
	implementation("org.aliquam:alq-session-api:0.0.1-DEV_BUILD")
	implementation("org.aliquam:alq-perms-api:0.0.1-DEV_BUILD")
	implementation("org.aliquam:alq-players-api:0.0.1-DEV_BUILD")
	implementation("org.aliquam:alq-worlds-api:0.0.1-DEV_BUILD")
	implementation("com.fasterxml.jackson.core:jackson-databind:2.13.0") // Needed to use TypeReference<>

	implementation("io.github.resilience4j:resilience4j-retry:1.7.1")

	implementation("ch.qos.logback:logback-classic:1.3.0-alpha10")
	implementation("net.kawinski.logging:nktrace:1.0.1.1")

	testImplementation(platform("org.junit:junit-bom:5.8.1"))
	testImplementation("org.junit.jupiter:junit-jupiter:5.8.1")
	testImplementation("org.hamcrest:hamcrest-library:2.2")
	testImplementation("org.mockito:mockito-junit-jupiter:4.0.0")
}

sonarqube {
	if (isJenkins) {
		properties {
			property("sonar.projectKey", "aliquam20_${artifactId}")
			property("sonar.organization", "aliquam")
			property("sonar.host.url", "https://sonarcloud.io")
			property("sonar.branch.name", branchName!!)
			property("sonar.coverage.jacoco.xmlReportPaths", "$projectDir/build/reports/jacoco/test/jacocoTestReport.xml")
		}
	}
}

tasks.test {
	finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
	useJUnitPlatform()

	testLogging {
		lifecycle {
			events = mutableSetOf(TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED)
			exceptionFormat = TestExceptionFormat.FULL
			showExceptions = true
			showCauses = true
			showStackTraces = true
			showStandardStreams = true
		}
		info.events = lifecycle.events
		info.exceptionFormat = lifecycle.exceptionFormat
	}

	val failedTests = mutableListOf<TestDescriptor>()
	val skippedTests = mutableListOf<TestDescriptor>()

	// See https://github.com/gradle/kotlin-dsl/issues/836
	addTestListener(object : TestListener {
		override fun beforeSuite(suite: TestDescriptor) {}
		override fun beforeTest(testDescriptor: TestDescriptor) {}
		override fun afterTest(testDescriptor: TestDescriptor, result: TestResult) {
			when (result.resultType) {
				TestResult.ResultType.FAILURE -> failedTests.add(testDescriptor)
				TestResult.ResultType.SKIPPED -> skippedTests.add(testDescriptor)
				else -> Unit
			}
		}

		override fun afterSuite(suite: TestDescriptor, result: TestResult) {
			if (suite.parent == null) { // root suite
				logger.lifecycle("----")
				logger.lifecycle("Test result: ${result.resultType}")
				logger.lifecycle(
					"Test summary: ${result.testCount} tests, " +
							"${result.successfulTestCount} succeeded, " +
							"${result.failedTestCount} failed, " +
							"${result.skippedTestCount} skipped")
				failedTests.takeIf { it.isNotEmpty() }?.prefixedSummary("\tFailed Tests")
				skippedTests.takeIf { it.isNotEmpty() }?.prefixedSummary("\tSkipped Tests:")
			}
		}

		private infix fun List<TestDescriptor>.prefixedSummary(subject: String) {
			logger.lifecycle(subject)
			forEach { test -> logger.lifecycle("\t\t${test.displayName()}") }
		}

		private fun TestDescriptor.displayName() = parent?.let { "${it.name} - $name" } ?: "$name"
	})
}

// cleanTest task doesn't have an accessor on tasks (when this blog post was written)
tasks.named("cleanTest") { group = "verification" }

tasks.jacocoTestReport {
	reports {
		xml.isEnabled = true
		csv.isEnabled = false
	}
	dependsOn(tasks.test) // tests are required to run before generating the report
}

