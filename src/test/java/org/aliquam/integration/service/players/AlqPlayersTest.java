package org.aliquam.integration.service.players;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.AlqJson;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.players.api.connector.AlqPlayersConnector;
import org.aliquam.players.api.model.AlqPlayer;
import org.aliquam.players.api.model.AlqPlayerIdentifiers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.TimeZone;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Slf4j
public class AlqPlayersTest {

    private final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2020, 10, 10, 12, 30, 30);
    private final AlqPlayersConnector connector = new AlqPlayersConnector(AlqIntegration.sessionManager);

    @Test
    public void basicPlayerCrudShouldWork() {
        UUID player1Id = UUID.fromString("00000001-1280-415a-9090-5796a1948231");
        AlqPlayer player1 = new AlqPlayer(player1Id, LOCAL_DATE_TIME, "IT_test1",
                UUID.randomUUID(), UUID.randomUUID(), 0d, TimeZone.getDefault(), "EN");
        UUID player2Id = UUID.fromString("00000002-1280-415a-9090-5796a1948231");
        AlqPlayer player2 = new AlqPlayer(player2Id, LOCAL_DATE_TIME, "IT_test2",
                UUID.randomUUID(), UUID.randomUUID(), 0d, TimeZone.getDefault(), "EN");

        log.info("player1: {}", player1);
        log.info("player1: {}", AlqJson.serialize(player1));

        Runnable cleanup = () -> {
            connector.deletePlayer(player1);
            connector.deletePlayer(player2);
        };
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.createPlayer(player1);
            connector.createPlayer(player2);
            assertThat(connector.getPlayer(player1Id), is(player1));
            assertThat(connector.getPlayer(player2Id), is(player2));

            player1.setLastName("IT_test1_changed");
            connector.patchPlayer(player1);
            assertThat(connector.getPlayer(player1Id), is(player1));

            cleanup.run();
            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.getPlayer(player1Id);
                connector.getPlayer(player2Id);
            });

        } finally {
            cleanup.run();
        }
    }

    @Test
    public void basicIdentifiersCrudShouldWork() {
        UUID player1Id = UUID.fromString("00000000-39eb-42a4-85e2-55a7dd4e7320");
        AlqPlayer player1 = new AlqPlayer(player1Id, LOCAL_DATE_TIME, "IT_test1",
                UUID.randomUUID(), UUID.randomUUID(), 0d, TimeZone.getDefault(), "EN");

        Runnable cleanup = () -> {
            connector.deletePlayer(player1);
        };
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.createPlayer(player1);

            AlqPlayerIdentifiers identifiers = connector.getIdentifiers(AlqPlayerIdentifiers.ofInternalId(player1Id));
            assertThat(identifiers.getInternalId(), is(player1Id));

            identifiers.setMinecraftId(UUID.fromString("00000000-8696-41ad-943c-c8b0940d02de"));
            connector.patchIdentifiers(identifiers);
            AlqPlayerIdentifiers patchedIdentifiers = connector.getIdentifiers(AlqPlayerIdentifiers.ofInternalId(player1Id));
            assertThat(patchedIdentifiers, is(identifiers));

            cleanup.run();

            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.getIdentifiers(AlqPlayerIdentifiers.ofInternalId(player1Id));
            });

        } finally {
            cleanup.run();
        }
    }

    @Test
    public void deleteShouldBeIdempotent() {
        connector.deletePlayer(UUID.fromString("00000000-2b88-4c2c-bb95-56f780a0c431"));
    }

}
