package org.aliquam.integration.service.perms;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.network.exception.client.AlqBadRequestResponseException;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.perms.api.connector.AlqPermsConnector;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.AlqPermTrack;
import org.aliquam.session.api.model.CreateContractRequest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;

@Slf4j
public class AlqPermsGroupTest extends AlqPermsTest {

    @Test
    public void basicGroupCrudShouldWork() {
        String name1 = "it_group1";
        String name2 = "it_group2";
        final AlqPermGroup group1 = new AlqPermGroup(name1, "disp1", 1);
        final AlqPermGroup group2 = new AlqPermGroup(name2, "disp2", 2);
        Runnable cleanup = () -> {
            connector.deleteGroup(group1);
            connector.deleteGroup(group2);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();
            List<AlqPermGroup> orgGroups = connector.listGroups();

            connector.putGroup(group1);
            List<AlqPermGroup> newGroups = connector.listGroups();
            assertThat(newGroups.size(), is(orgGroups.size() + 1));
            assertThat(group1, is(in(newGroups)));

            connector.putGroup(group2);
            newGroups = connector.listGroups();
            assertThat(newGroups.size(), is(orgGroups.size() + 2));
            assertThat(group2, is(in(newGroups)));

            AlqPermGroup getGroup1 = connector.getGroup(name1);
            assertThat(getGroup1, is(group1));
            AlqPermGroup getGroup2 = connector.getGroup(name2);
            assertThat(getGroup2, is(group2));

            cleanup.run();
            newGroups = connector.listGroups();
            assertThat(newGroups.size(), is(orgGroups.size()));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void createGroupIsIdempotent() {
        final AlqPermGroup group = new AlqPermGroup("it_group_idempotent", "dispname", 1);
        try (NkTrace ignored = NkTrace.info(log)) {
            connector.putGroup(group);
            connector.putGroup(group);
        } finally {
            connector.deleteGroup(group);
        }
    }

    @Test
    public void createGroupThrows400IfMissingName() {
        try (NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.putGroup(new AlqPermGroup(null, "dispname", 1));
            });
        }
    }

    @Test
    public void getGroupThrows404IfNotFound() {
        try (NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.getGroup("nonexistinggroup");
            });
        }
    }

    @Test
    public void deleteGroupIsIdempotent() {
        try (NkTrace ignored = NkTrace.info(log)) {
            connector.deleteGroup("nonexistinggroup");
        }
    }

    @Test
    public void basicGroupInheritanceWorks() {
        final AlqPermGroup group1 = new AlqPermGroup("it_basic_inh_group1", "disp1", 1);
        final AlqPermGroup group2 = new AlqPermGroup("it_basic_inh_group2", "disp2", 2);

        Runnable cleanup = () -> {
            connector.deleteGroup(group1);
            connector.deleteGroup(group2);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.putGroup(group1);
            connector.putGroup(group2);
            connector.addGroupInheritedGroup(group1, group2, DEFAULT_CONTEXT);

            List<AlqPermGroup> inheritedGroups = connector.getGroupInheritedGroups(group1, DEFAULT_CONTEXT);
            assertThat(inheritedGroups.size(), is(1));
            assertThat(group2, is(in(inheritedGroups)));

            connector.removeGroupInheritedGroup(group1, group2, DEFAULT_CONTEXT);
            inheritedGroups = connector.getGroupInheritedGroups(group1, DEFAULT_CONTEXT);
            assertThat(inheritedGroups.size(), is(0));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void contextualGroupInheritanceWorks() {
        final AlqPermGroup group1 = new AlqPermGroup("it_context_inh_group1", "disp1", 1);
        final AlqPermGroup group2 = new AlqPermGroup("it_context_inh_group2", "disp2", 2);

        Runnable cleanup = () -> {
            connector.deleteGroup(group1);
            connector.deleteGroup(group2);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.putGroup(group1);
            connector.putGroup(group2);
            connector.addGroupInheritedGroup(group1, group2, CUSTOM_CONTEXT);

            List<AlqPermGroup> inheritedGroups = connector.getGroupInheritedGroups(group1, DEFAULT_CONTEXT);
            assertThat(inheritedGroups.size(), is(0));

            inheritedGroups = connector.getGroupInheritedGroups(group1, CUSTOM_CONTEXT);
            assertThat(inheritedGroups.size(), is(1));
            assertThat(group2, is(in(inheritedGroups)));

            connector.removeGroupInheritedGroup(group1, group2, CUSTOM_CONTEXT);
            inheritedGroups = connector.getGroupInheritedGroups(group1, CUSTOM_CONTEXT);
            assertThat(inheritedGroups.size(), is(0));

        } finally {
            cleanup.run();
        }
    }

    @Test
    public void basicGroupPermissionsWorks() {
        final AlqPermGroup group = new AlqPermGroup("it_basic_perm_group1", "disp1", 1);
        final AlqPerm perm1 = new AlqPerm("alq.test.group.it_basic_perm1", DEFAULT_CONTEXT);
        final AlqPerm perm2 = new AlqPerm("alq.test.group.it_basic_perm2", CUSTOM_CONTEXT);

        Runnable cleanup = () -> {
            connector.deleteGroup(group);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.putGroup(group);
            connector.addGroupPermission(group, perm1);
            connector.addGroupPermission(group, perm2);

            List<AlqPerm> permissions = connector.listGroupPermissions(group);
            assertThat(permissions.size(), is(2));
            assertThat(perm1, is(in(permissions)));
            assertThat(perm2, is(in(permissions)));

            connector.removeGroupPermission(group, perm1);
            connector.removeGroupPermission(group, perm2);
            permissions = connector.listGroupPermissions(group);
            assertThat(permissions.size(), is(0));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void sameGroupPermissionsWorksOnDifferentContexts() {
        final AlqPermGroup group = new AlqPermGroup("it_basic_perm_group1", "disp1", 1);
        final AlqPerm perm1 = new AlqPerm("alq.test.group.it_same_perm", CUSTOM_CONTEXT);
        final AlqPerm perm2 = new AlqPerm("alq.test.group.it_same_perm", CUSTOM_CONTEXT2);

        Runnable cleanup = () -> {
            connector.deleteGroup(group);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.putGroup(group);
            connector.addGroupPermission(group, perm1);
            connector.addGroupPermission(group, perm2);

            List<AlqPerm> permissions = connector.listGroupPermissions(group);
            assertThat(permissions.size(), is(2));
            assertThat(perm1, is(in(permissions)));
            assertThat(perm2, is(in(permissions)));

            connector.removeGroupPermission(group, perm1);
            connector.removeGroupPermission(group, perm2);
            permissions = connector.listGroupPermissions(group);
            assertThat(permissions.size(), is(0));
        } finally {
            cleanup.run();
        }
    }

}
