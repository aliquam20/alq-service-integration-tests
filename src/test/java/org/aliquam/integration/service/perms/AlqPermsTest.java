package org.aliquam.integration.service.perms;

import lombok.extern.slf4j.Slf4j;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.perms.api.connector.AlqPermsConnector;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.AlqPermTrack;

@Slf4j
public class AlqPermsTest {

    public static final AlqPermGroup GROUP_DEFAULT = new AlqPermGroup("default", null, null);
    public static final AlqPermGroup GROUP_PLAYER = new AlqPermGroup("it_player", null, null);
    public static final AlqPermGroup GROUP_BUILDER = new AlqPermGroup("it_builder", null, null);
    public static final AlqPermGroup GROUP_ARCHITECT = new AlqPermGroup("it_architect", null, null);
    public static final AlqPermTrack TRACK_BUILDER = AlqPermTrack.create("it_builder_track",
            GROUP_DEFAULT, GROUP_PLAYER, GROUP_BUILDER, GROUP_ARCHITECT);

    public static final AlqPermContext DEFAULT_CONTEXT = new AlqPermContext();
    public static final AlqPermContext CUSTOM_CONTEXT = new AlqPermContext("it_server", "it_world1");
    public static final AlqPermContext CUSTOM_CONTEXT2 = new AlqPermContext("it_server", "it_world2");

    public static final AlqPermsConnector connector = new AlqPermsConnector(AlqIntegration.sessionManager);

}
