package org.aliquam.integration.service.perms;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.network.exception.client.AlqBadRequestResponseException;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.AlqPermUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;

@Slf4j
public class AlqPermsUserTest extends AlqPermsTest {

    @Test
    public void getUserReturnsDefaultIfNotFound() {
        try (NkTrace ignored = NkTrace.info(log)) {
            UUID id = UUID.fromString("00000000-5f9e-4397-ba2d-4c51f3da99c6");
            AlqPermUser user = connector.getUser(id);
            assertThat(user.getId(), is(id));
        }
    }

    @Test
    public void getUserThrows400IfMissingId() {
        try (NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.getUser(null);
            });
        }
    }

    @Test
    public void deleteUserIsIdempotent() {
        try (NkTrace ignored = NkTrace.info(log)) {
            UUID id = UUID.fromString("00000000-a064-4af5-b83e-7693132b67b7");
            connector.deleteUser(id);
        }
    }

    @Test
    public void deleteUserThrows400IfMissingName() {
        try (NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.deleteUser(null);
            });
        }
    }

    @Test
    public void basicUserInheritanceWorks() {
        AlqPermGroup group = new AlqPermGroup("it_basic_inh_user", "disp1", 1);
        UUID userId = UUID.fromString("00000000-8cd6-4115-ade2-67f1a501e81c");

        Runnable cleanup = () -> {
            connector.deleteGroup(group);
            connector.deleteUser(userId);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.putGroup(group);
            connector.addUserInheritedGroup(userId, group, DEFAULT_CONTEXT);

            List<AlqPermGroup> inheritedGroups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(inheritedGroups.size(), is(2));
            assertThat(GROUP_DEFAULT, is(in(inheritedGroups)));
            assertThat(group, is(in(inheritedGroups)));

            connector.removeUserInheritedGroup(userId, group, DEFAULT_CONTEXT);
            inheritedGroups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(inheritedGroups.size(), is(1));
            assertThat(GROUP_DEFAULT, is(in(inheritedGroups)));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void contextualGroupInheritanceWorks() {
        AlqPermGroup group = new AlqPermGroup("it_context_inh_user", "disp1", 1);
        UUID userId = UUID.fromString("00000000-9ad9-4f21-89d3-0959fb5d79b2");

        Runnable cleanup = () -> {
            connector.deleteGroup(group);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.putGroup(group);
            connector.addUserInheritedGroup(userId, group, CUSTOM_CONTEXT);

            List<AlqPermGroup> inheritedGroups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(inheritedGroups.size(), is(1));
            assertThat(GROUP_DEFAULT, is(in(inheritedGroups)));

            inheritedGroups = connector.getUserInheritedGroups(userId, CUSTOM_CONTEXT);
            assertThat(inheritedGroups.size(), is(2));
            assertThat(GROUP_DEFAULT, is(in(inheritedGroups)));
            assertThat(group, is(in(inheritedGroups)));

            connector.removeUserInheritedGroup(userId, group, CUSTOM_CONTEXT);
            inheritedGroups = connector.getUserInheritedGroups(userId, CUSTOM_CONTEXT);
            assertThat(inheritedGroups.size(), is(1));
            assertThat(GROUP_DEFAULT, is(in(inheritedGroups)));

        } finally {
            cleanup.run();
        }
    }

    @Test
    public void basicUserPermissionsWorks() {
        final UUID userId = UUID.fromString("00000000-a5d0-4df9-8fea-81678e4eaad7");
        final AlqPerm perm1 = new AlqPerm("alq.test.user.it_basic_perm1", DEFAULT_CONTEXT);
        final AlqPerm perm2 = new AlqPerm("alq.test.user.it_basic_perm2", CUSTOM_CONTEXT);

        Runnable cleanup = () -> {
            connector.deleteUser(userId);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.addUserPermission(userId, perm1);
            connector.addUserPermission(userId, perm2);

            List<AlqPerm> permissions = connector.listUserPermissions(userId);
            assertThat(permissions.size(), is(2));
            assertThat(perm1, is(in(permissions)));
            assertThat(perm2, is(in(permissions)));

            connector.removeUserPermission(userId, perm1);
            connector.removeUserPermission(userId, perm2);
            permissions = connector.listUserPermissions(userId);
            assertThat(permissions.size(), is(0));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void sameUserPermissionsWorksOnDifferentContexts() {
        final UUID userId = UUID.fromString("00000000-5d0e-4a51-8722-7c73e55f5997");
        final AlqPerm perm1 = new AlqPerm("alq.test.user.it_same_perm", CUSTOM_CONTEXT);
        final AlqPerm perm2 = new AlqPerm("alq.test.user.it_same_perm", CUSTOM_CONTEXT2);

        Runnable cleanup = () -> {
            connector.deleteUser(userId);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.addUserPermission(userId, perm1);
            connector.addUserPermission(userId, perm2);

            List<AlqPerm> permissions = connector.listUserPermissions(userId);
            assertThat(permissions.size(), is(2));
            assertThat(perm1, is(in(permissions)));
            assertThat(perm2, is(in(permissions)));

            connector.removeUserPermission(userId, perm1);
            connector.removeUserPermission(userId, perm2);
            permissions = connector.listUserPermissions(userId);
            assertThat(permissions.size(), is(0));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void userHasPermissionWorks() {
        final UUID userId = UUID.fromString("00000000-6700-462b-b894-2a5159bf621e");
        final AlqPerm perm1 = new AlqPerm("alq.test.user.it_has_perm1", DEFAULT_CONTEXT);
        final AlqPerm perm2 = new AlqPerm("alq.test.user.it_has_perm2", CUSTOM_CONTEXT);
        final AlqPerm perm3 = new AlqPerm("alq.test.user.it_has_perm3", CUSTOM_CONTEXT2);

        Runnable cleanup = () -> {
            connector.deleteUser(userId);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.addUserPermission(userId, perm1);
            connector.addUserPermission(userId, perm2);
            connector.addUserPermission(userId, perm3);
            assertThat(connector.hasUserPermission(userId, perm1.getPermission(), DEFAULT_CONTEXT), is(true));
            assertThat(connector.hasUserPermission(userId, perm1.getPermission(), CUSTOM_CONTEXT), is(true));
            assertThat(connector.hasUserPermission(userId, perm1.getPermission(), CUSTOM_CONTEXT2), is(true));
            assertThat(connector.hasUserPermission(userId, perm2.getPermission(), DEFAULT_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm2.getPermission(), CUSTOM_CONTEXT), is(true));
            assertThat(connector.hasUserPermission(userId, perm2.getPermission(), CUSTOM_CONTEXT2), is(false));
            assertThat(connector.hasUserPermission(userId, perm3.getPermission(), DEFAULT_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm3.getPermission(), CUSTOM_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm3.getPermission(), CUSTOM_CONTEXT2), is(true));

            connector.removeUserPermission(userId, perm1);
            connector.removeUserPermission(userId, perm2);
            connector.removeUserPermission(userId, perm3);
            assertThat(connector.hasUserPermission(userId, perm1.getPermission(), DEFAULT_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm1.getPermission(), CUSTOM_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm1.getPermission(), CUSTOM_CONTEXT2), is(false));
            assertThat(connector.hasUserPermission(userId, perm2.getPermission(), DEFAULT_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm2.getPermission(), CUSTOM_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm2.getPermission(), CUSTOM_CONTEXT2), is(false));
            assertThat(connector.hasUserPermission(userId, perm3.getPermission(), DEFAULT_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm3.getPermission(), CUSTOM_CONTEXT), is(false));
            assertThat(connector.hasUserPermission(userId, perm3.getPermission(), CUSTOM_CONTEXT2), is(false));
        } finally {
            cleanup.run();
        }
    }

}
