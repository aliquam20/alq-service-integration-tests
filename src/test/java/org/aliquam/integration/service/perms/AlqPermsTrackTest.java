package org.aliquam.integration.service.perms;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.network.exception.client.AlqBadRequestResponseException;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.cum.network.exception.server.AlqInternalServerErrorResponseException;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.AlqPermTrack;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;

@Slf4j
public class AlqPermsTrackTest extends AlqPermsTest {

    @BeforeAll
    public static void prepareTestEnv() {
        connector.putGroup(GROUP_PLAYER);
        connector.putGroup(GROUP_BUILDER);
        connector.putGroup(GROUP_ARCHITECT);
    }

    @AfterAll
    public static void cleanupTestEnv() {
        connector.deleteGroup(GROUP_PLAYER);
        connector.deleteGroup(GROUP_BUILDER);
        connector.deleteGroup(GROUP_ARCHITECT);
    }

    @Test
    public void basicTrackCrudShouldWork() {
        String name1 = "it_track1";
        String name2 = "it_track2";
        final AlqPermTrack track1 = AlqPermTrack.create(name1, GROUP_DEFAULT, GROUP_PLAYER, GROUP_BUILDER, GROUP_ARCHITECT);
        final AlqPermTrack track2 = AlqPermTrack.create(name2, GROUP_DEFAULT, GROUP_PLAYER, GROUP_BUILDER, GROUP_ARCHITECT);
        Runnable cleanup = () -> {
            connector.deleteTrack(track1);
            connector.deleteTrack(track2);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            List<AlqPermTrack> orgTracks = connector.listTracks();

            connector.putTrack(track1);
            List<AlqPermTrack> newTracks = connector.listTracks();
            assertThat(newTracks.size(), is(orgTracks.size() + 1));
            assertThat(track1, is(in(newTracks)));

            connector.putTrack(track2);
            newTracks = connector.listTracks();
            assertThat(newTracks.size(), is(orgTracks.size() + 2));
            assertThat(track2, is(in(newTracks)));

            AlqPermTrack getTrack1 = connector.getTrack(name1);
            assertThat(getTrack1, is(track1));
            AlqPermTrack getTrack2 = connector.getTrack(name2);
            assertThat(getTrack2, is(track2));

            cleanup.run();
            newTracks = connector.listTracks();
            assertThat(newTracks.size(), is(orgTracks.size()));
        } finally {
            cleanup.run();
        }
    }


    @Test
    public void createTrackIsIdempotent() {
        final AlqPermTrack track = new AlqPermTrack("it_track_idempotent", Collections.emptyList());
        try (NkTrace ignored = NkTrace.info(log)) {
            connector.putTrack(track);
            connector.putTrack(track);
        } finally {
            connector.deleteTrack(track);
        }
    }

    @Test
    public void createTrackThrows400IfMissingName() {
        try (NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.putTrack(new AlqPermTrack(null, Collections.emptyList()));
            });
        }
    }

    @Test
    public void getTrackThrows404IfNotFound() {
        try (NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.getTrack("nonexistingtrack");
            });
        }
    }

    @Test
    public void deleteTrackIsIdempotent() {
        try (NkTrace ignored = NkTrace.info(log)) {
            connector.deleteTrack("nonexistingtrack");
        }
    }

    @Test
    public void promoteAndDemoteShouldWork() {
        UUID userId = UUID.fromString("00000000-c87e-4de7-9f29-982781e55d31");
        Runnable cleanup = () -> {
            connector.deleteUser(userId);
            connector.deleteTrack(TRACK_BUILDER);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();
            connector.putTrack(TRACK_BUILDER);

            List<AlqPermGroup> groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_DEFAULT, is(in(groups)));

            connector.promote(userId, GROUP_DEFAULT, TRACK_BUILDER);
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_PLAYER, is(in(groups)));

            connector.promote(userId, GROUP_PLAYER, TRACK_BUILDER);
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_BUILDER, is(in(groups)));

            connector.promote(userId, GROUP_BUILDER, TRACK_BUILDER);
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_ARCHITECT, is(in(groups)));

            Assertions.assertThrows(AlqInternalServerErrorResponseException.class, () -> {
                connector.promote(userId, GROUP_ARCHITECT, TRACK_BUILDER);
            });
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_ARCHITECT, is(in(groups)));

            connector.demote(userId, GROUP_ARCHITECT, TRACK_BUILDER);
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_BUILDER, is(in(groups)));

            connector.demote(userId, GROUP_BUILDER, TRACK_BUILDER);
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_PLAYER, is(in(groups)));

            connector.demote(userId, GROUP_PLAYER, TRACK_BUILDER);
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_DEFAULT, is(in(groups)));

            Assertions.assertThrows(AlqInternalServerErrorResponseException.class, () -> {
                connector.demote(userId, GROUP_DEFAULT, TRACK_BUILDER);
            });
            groups = connector.getUserInheritedGroups(userId, DEFAULT_CONTEXT);
            assertThat(groups.size(), is(1));
            assertThat(GROUP_DEFAULT, is(in(groups)));

        } finally {
            cleanup.run();
        }
    }

}
