package org.aliquam.integration.service.config;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.config.api.connector.AlqConfigConnector;
import org.aliquam.config.api.model.Config;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.cum.network.exception.client.AlqUnauthorizedResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.session.api.connector.AlqDummySessionManager;
import org.aliquam.session.api.model.Contract;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;

@Slf4j
public class AlqConfigTest {

    private final AlqConfigConnector connector = new AlqConfigConnector(AlqIntegration.sessionManager);

    @Test
    public void basicCrudShouldWork() {
        String value1 = "value1";
        String value2 = "value2";
        final Config config1 = new Config("alq.test.key1", value1);
        final Config config2 = new Config("alq.test.key2", value2);
        Runnable cleanup = () -> {
            connector.delete(config1);
            connector.delete(config2);
        };
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();
            List<Config> orgConfigs = connector.list();

            connector.set(config1);
            List<Config> newConfigs = connector.list();
            assertThat(newConfigs.size(), is(orgConfigs.size() + 1));
            assertThat(config1, is(in(newConfigs)));

            connector.set(config2);
            newConfigs = connector.list();
            assertThat(newConfigs.size(), is(orgConfigs.size() + 2));
            assertThat(config2, is(in(newConfigs)));

            String getValue = connector.get(config1.getKey(), String.class);
            assertThat(getValue, is(value1));

            cleanup.run();
            newConfigs = connector.list();
            assertThat(newConfigs.size(), is(orgConfigs.size()));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void internalConfigAvailableOnlyToInternalSessions() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Integer response = connector.get("the-answer", Integer.class);
            assertThat(response, is(42));

            Assertions.assertThrows(AlqUnauthorizedResponseException.class, () -> {
                AlqConfigConnector dummyConnector = new AlqConfigConnector(new AlqDummySessionManager());
                dummyConnector.get("the-answer", Integer.class);
            });
        }
    }

    @Test
    public void externalConfigNotAvailableWithoutSession() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqUnauthorizedResponseException.class, () -> {
                AlqConfigConnector dummyConnector = new AlqConfigConnector(new AlqDummySessionManager());
                dummyConnector.get("ping", Integer.class);
            });
        }
    }

    @Test
    public void getAndSetForDifferentDataTypes() {
        String KEY = "alq.test.key";

        Runnable cleanup = () -> connector.delete(KEY);
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            connector.set(KEY, "test");
            assertThat(connector.get(KEY, String.class), is("test"));

            connector.set(KEY, 123);
            assertThat(connector.get(KEY, Integer.class), is(123));

            connector.set(KEY, 123.45);
            assertThat(connector.get(KEY, Double.class), is(123.45));

            connector.set(KEY, true);
            assertThat(connector.get(KEY, Boolean.class), is(true));

            List<String> list = Arrays.asList("A", "B", "C");
            connector.set(KEY, list);
            assertThat(connector.get(KEY, new TypeReference<List<String>>() {}), contains("A", "B", "C"));

            Contract contract = new Contract(UUID.randomUUID(), "test", true);
            connector.set(KEY, contract);
            assertThat(connector.get(KEY, Contract.class), is(contract));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void deleteShouldBeIdempotent() {
        connector.delete("alq.test.notexisting");
    }

    @Test
    public void exceptionThrownWhenEntryIsMissing() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.get("alq.test.notexisting", Object.class);
            });
        }
    }

    @Test
    public void getOrSetWorks() {
        String KEY = "alq.test.key";

        Runnable cleanup = () -> {
            connector.delete(KEY);
        };
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            String value = connector.getOrSet(KEY, "default", String.class);
            assertThat(value, is("default"));

            connector.set(KEY, "not default");

            value = connector.getOrSet(KEY, "default", String.class);
            assertThat(value, is("not default"));
        } finally {
            cleanup.run();
        }
    }

}
