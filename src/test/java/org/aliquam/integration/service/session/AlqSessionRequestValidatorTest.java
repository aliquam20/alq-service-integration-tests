package org.aliquam.integration.service.session;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.network.exception.client.AlqForbiddenResponseException;
import org.aliquam.cum.network.exception.client.AlqUnauthorizedResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.session.api.connector.AlqSessionConnector;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.aliquam.session.api.model.CreateContractRequest;
import org.aliquam.session.api.model.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Slf4j
public class AlqSessionRequestValidatorTest {

    private final AlqSessionConnector sessionConnector = new AlqSessionConnector(AlqIntegration.sessionManager);

    @Test
    public void requestIsRejected_whenSessionIsMissing() {
        try(NkTrace ignored = NkTrace.info(log)) {
            UUID SESSION_ID = UUID.fromString("00000000-d1e5-4761-829a-c22bea0f0abb");
            AlqSessionManager sessionManager = mock(AlqSessionManager.class);
            when(sessionManager.getSessionId()).thenReturn(SESSION_ID);
            AlqSessionConnector connector = new AlqSessionConnector(sessionManager);

            Assertions.assertThrows(AlqUnauthorizedResponseException.class, connector::listContracts);
        }
    }

    @Test
    public void requestIsRejected_whenSessionShouldBeInternal() {
        final UUID EXT_CONTRACT_ID = UUID.fromString("00000000-f57d-448c-9323-a61196a01be4");
        final String EXT_CONTRACT_PASS = "123456";
        try(NkTrace ignored = NkTrace.info(log)) {
            sessionConnector.putContract(EXT_CONTRACT_ID, "test", false, EXT_CONTRACT_PASS);
            Session session = sessionConnector.createSession(EXT_CONTRACT_ID, EXT_CONTRACT_PASS, UUID.randomUUID());
            AlqSessionManager sessionManager = mock(AlqSessionManager.class);
            when(sessionManager.getSessionId()).thenReturn(session.getId());
            AlqSessionConnector connector = new AlqSessionConnector(sessionManager);

            Assertions.assertThrows(AlqForbiddenResponseException.class, connector::listContracts);
        } finally {
            sessionConnector.deleteContract(EXT_CONTRACT_ID);
        }
    }


}
