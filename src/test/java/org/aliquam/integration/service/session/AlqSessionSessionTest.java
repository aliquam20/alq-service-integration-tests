package org.aliquam.integration.service.session;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.network.exception.client.AlqBadRequestResponseException;
import org.aliquam.cum.network.exception.client.AlqForbiddenResponseException;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.session.api.connector.AlqSessionConnector;
import org.aliquam.session.api.model.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
public class AlqSessionSessionTest {

    private static final UUID ADMIN_CONTRACT_ID = UUID.fromString("f36e4d14-4a1a-458d-9085-ff8a313f5bea");
    private static final String ADMIN_CONTRACT_PASS = "BSD8aGA3BmUAx2WrpTyFLmoVQxBkxInI";
    private static final UUID EXT_CONTRACT_ID = UUID.fromString("a3994e73-23aa-4046-a01b-fe5917e479bb");
    private static final String EXT_CONTRACT_PASS = "ItVX73uS5VSnjPdE0dRqh0lmeQo8BxHl";
    private final AlqSessionConnector connector = new AlqSessionConnector(AlqIntegration.sessionManager);

    @Test
    public void createSessionShouldWork_forInternalContract() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Session session = connector.createSession(ADMIN_CONTRACT_ID, ADMIN_CONTRACT_PASS, null);
            assertThat(session.getId(), is(notNullValue()));
        }
    }

    @Test
    public void createSessionShouldWork_forExternalContract() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Session session = connector.createSession(EXT_CONTRACT_ID, EXT_CONTRACT_PASS, UUID.randomUUID());
            assertThat(session.getId(), is(notNullValue()));
        }
    }

    @Test
    public void createdSessionShouldNotBeExpired() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Session session = connector.createSession(ADMIN_CONTRACT_ID, ADMIN_CONTRACT_PASS, null);
            assertTrue(session.getExpires().isAfter(LocalDateTime.now()));
        }
    }

    @Test
    public void createSessionIsNotIdempotent() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Session session1 = connector.createSession(ADMIN_CONTRACT_ID, ADMIN_CONTRACT_PASS, null);
            Session session2 = connector.createSession(ADMIN_CONTRACT_ID, ADMIN_CONTRACT_PASS, null);
            assertThat(session1.getId(), is(not(session2.getId())));
        }
    }

    @Test
    public void createSessionRejectsInvalidRequest_missingContract() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.createSession(null, ADMIN_CONTRACT_PASS, null);
            });
        }
    }

    @Test
    public void createSessionRejectsInvalidRequest_InvalidPassword() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqForbiddenResponseException.class, () -> {
                connector.createSession(ADMIN_CONTRACT_ID, "invalid password", null);
            });
        }
    }

    @Test
    public void createSessionRejectsInvalidRequest_MissingTenantWhenUsingInternalContract() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqForbiddenResponseException.class, () -> {
                connector.createSession(EXT_CONTRACT_ID, EXT_CONTRACT_PASS, null);
            });
        }
    }

    @Test
    public void sessionReadWorks() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Session session1 = connector.createSession(ADMIN_CONTRACT_ID, ADMIN_CONTRACT_PASS, null);
            Session session2 = connector.getSession(session1.getId());
            assertThat(session1, is(session2));
        }
    }

    @Test
    public void sessionReadRejectsInvalidRequest_MissingId() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.getSession(null);
            });
        }
    }

    @Test
    public void sessionReadRejectsInvalidRequest_SessionDoesNotExist() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.getSession(UUID.fromString("00000000-e4c8-474f-9be8-703088fc5d3c"));
            });
        }
    }

    @Test
    public void sessionExtendWorks() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Session session1 = connector.createSession(ADMIN_CONTRACT_ID, ADMIN_CONTRACT_PASS, null);
            Session session2 = connector.extendSession(session1.getId());
            assertThat(session1.getId(), is(session2.getId()));
            assertTrue(session1.getExpires().isBefore(session2.getExpires())); // Just by a few milliseconds but still :)
        }
    }

    @Test
    public void sessionExtendRejectsInvalidRequest_missingId() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.extendSession((UUID) null);
            });
        }
    }

    @Test
    public void sessionExtendReturns404_whenSessionDoesNotExist() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.extendSession(UUID.fromString("00000000-ccf6-44ed-bfb4-b7cc4aa98c4e"));
            });
        }
    }









}
