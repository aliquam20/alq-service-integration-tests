package org.aliquam.integration.service.session;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.network.exception.client.AlqBadRequestResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.session.api.connector.AlqSessionConnector;
import org.aliquam.session.api.model.Contract;
import org.aliquam.session.api.model.CreateContractRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@Slf4j
public class AlqSessionContractTest {

    private final AlqSessionConnector connector = new AlqSessionConnector(AlqIntegration.sessionManager);

    @Test
    public void basicCrudShouldWork() {
        final Contract contract1 = new Contract(UUID.fromString("00000001-72ec-4e0f-b631-c4423b6c1e0d"), "test1", false);
        final Contract contract2 = new Contract(UUID.fromString("00000002-72ec-4e0f-b631-c4423b6c1e0d"), "test2", false);
        final CreateContractRequest createContract1Request = new CreateContractRequest(contract1, "password1");
        final CreateContractRequest createContract2Request = new CreateContractRequest(contract2, "password2");
        Runnable cleanup = () -> {
            connector.deleteContract(contract1);
            connector.deleteContract(contract2);
        };
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();
            List<Contract> orgContracts = connector.listContracts();

            connector.putContract(createContract1Request);
            List<Contract> newContracts = connector.listContracts();
            assertThat(newContracts.size(), is(orgContracts.size() + 1));
            assertThat(contract1, is(in(newContracts)));

            connector.putContract(createContract2Request);
            newContracts = connector.listContracts();
            assertThat(newContracts.size(), is(orgContracts.size() + 2));
            assertThat(contract2, is(in(newContracts)));

            cleanup.run();
            newContracts = connector.listContracts();
            assertThat(newContracts.size(), is(orgContracts.size()));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void putShouldBeIdempotent() {
        final Contract contract = new Contract(UUID.fromString("00000000-72ec-4e0f-b631-c4423b6c1e0d"), "test", false);
        final CreateContractRequest createContractRequest = new CreateContractRequest(contract, "password");
        Runnable cleanup = () -> connector.deleteContract(contract);

        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();
            List<Contract> orgContracts = connector.listContracts();

            connector.putContract(createContractRequest);
            connector.putContract(createContractRequest);
            List<Contract> newContracts = connector.listContracts();
            assertThat(newContracts.size(), is(orgContracts.size() + 1));
        } finally {
            cleanup.run();
        }
    }

    @Test
    public void deleteShouldBeIdempotent() {
        final Contract contract = new Contract(UUID.fromString("00000000-72ec-4e0f-b631-c4423b6c1e0d"), "test", false);
        connector.deleteContract(contract);
        connector.deleteContract(contract);
    }

    @Test
    public void invalidRequestsAreRejected_putContractWithNullId() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.putContract(new CreateContractRequest(null, "name", true, "password"));
            });
        }
    }

    @Test
    public void invalidRequestsAreRejected_putContractWithNullName() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.putContract(new CreateContractRequest(UUID.randomUUID(), null, true, "password"));
            });
        }
    }

    @Test
    public void invalidRequestsAreRejected_putContractWithEmptyName() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.putContract(new CreateContractRequest(UUID.randomUUID(), "", true, "password"));
            });
        }
    }

    @Test
    public void invalidRequestsAreRejected_deleteContractWithNullId() {
        try(NkTrace ignored = NkTrace.info(log)) {
            Assertions.assertThrows(AlqBadRequestResponseException.class, () -> {
                connector.deleteContract((UUID) null);
            });
        }
    }



}
