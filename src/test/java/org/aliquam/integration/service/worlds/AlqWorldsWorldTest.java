package org.aliquam.integration.service.worlds;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.worlds.api.communication.PatchWorldRequest;
import org.aliquam.worlds.api.connector.AlqWorldsConnector;
import org.aliquam.worlds.api.model.AlqWorld;
import org.aliquam.worlds.api.model.AlqWorldAccess;
import org.aliquam.worlds.api.model.AlqWorldChat;
import org.aliquam.worlds.api.model.AlqWorldDifficulty;
import org.aliquam.worlds.api.model.AlqWorldGamemode;
import org.aliquam.worlds.api.model.AlqWorldProjectiles;
import org.aliquam.worlds.api.model.AlqWorldSettings;
import org.aliquam.worlds.api.model.AlqWorldTimeLock;
import org.aliquam.worlds.api.model.AlqWorldWeather;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Not;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

@Slf4j
public class AlqWorldsWorldTest {

    private final AlqWorldsConnector connector = new AlqWorldsConnector(AlqIntegration.sessionManager);

    @Test
    public void basicCrudShouldWork() {
        UUID obj1Id = UUID.fromString("00000001-5a31-4a95-8432-2ea2fa788906");
        UUID obj1OwnerId = UUID.fromString("00000002-5a31-4a95-8432-2ea2fa788906");
        String obj1Name = "world1";
        UUID obj2Id = UUID.fromString("00000003-5a31-4a95-8432-2ea2fa788906");
        UUID obj2OwnerId = UUID.fromString("00000004-5a31-4a95-8432-2ea2fa788906");
        String obj2Name = "world2";

        UUID obj1ChatGroupId = UUID.fromString("00000001-c36a-4cdd-8507-4026bc73fa45");
        UUID obj1InventoryGroupId = UUID.fromString("00000002-c36a-4cdd-8507-4026bc73fa45");

        Runnable cleanup = () -> {
            connector.deleteWorld(obj1Id);
            connector.deleteWorld(obj2Id);
        };
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            AlqWorld created1 = connector.createWorld(obj1Id, obj1OwnerId, obj1Name);
            AlqWorld world1 = connector.getWorld(obj1Id);
            assertThat(created1, is(world1));
            assertThat(world1.getId(), is(obj1Id));
            assertThat(world1.getOwner(), is(obj1OwnerId));
            assertThat(world1.getName(), is(obj1Name));
            assertDefaultWorldSettings(world1.getSettings());

            AlqWorld created2 = connector.createWorld(obj2Id, obj2OwnerId, obj2Name);
            AlqWorld world2 = connector.getWorld(obj2Id);
            assertThat(created2, is(world2));
            assertThat(world2.getId(), is(obj2Id));
            assertThat(world2.getOwner(), is(obj2OwnerId));
            assertThat(world2.getName(), is(obj2Name));

            PatchWorldRequest emptyPatchWorldRequest = new PatchWorldRequest();
            connector.updateWorld(obj1Id, emptyPatchWorldRequest);
            AlqWorld changed1 = connector.getWorld(obj1Id);
            assertDefaultWorldSettings(changed1.getSettings());

            PatchWorldRequest patchWorldRequest = new PatchWorldRequest();
            patchWorldRequest.setName("changed");
            patchWorldRequest.getSettings().setAccess(AlqWorldAccess.Private);
            patchWorldRequest.getSettings().setChat(AlqWorldChat.Private);
            patchWorldRequest.getSettings().setPrivateChatGroup(obj1ChatGroupId);
            patchWorldRequest.getSettings().setGamemode(AlqWorldGamemode.Survival);
            patchWorldRequest.getSettings().setSpawn(new PatchWorldRequest.SettingsSpawn(1.d, 2.d, 3.d, 4.d, 5.d));
            patchWorldRequest.getSettings().setFindSafeSpawnLocation(false);
            patchWorldRequest.getSettings().setTimeLock(AlqWorldTimeLock.Night);
            patchWorldRequest.getSettings().setWeather(AlqWorldWeather.Rain);
            patchWorldRequest.getSettings().setPhysics(true);
            patchWorldRequest.getSettings().setMobs(true);
            patchWorldRequest.getSettings().setInteraction(false);
            patchWorldRequest.getSettings().setResourcepack("respack");
            patchWorldRequest.getSettings().setWelcomeMessage("welcome");
            patchWorldRequest.getSettings().setFarewellMessage("farewell");
            patchWorldRequest.getSettings().setDifficulty(AlqWorldDifficulty.Hard);
            patchWorldRequest.getSettings().setDrops(true);
            patchWorldRequest.getSettings().setProjectiles(AlqWorldProjectiles.Allowed);
            patchWorldRequest.getSettings().setExplosions(true);
            patchWorldRequest.getSettings().setHealthRegen(false);
            patchWorldRequest.getSettings().setKeepInventory(false);
            patchWorldRequest.getSettings().setSurvivalInventoryGroup(obj1InventoryGroupId);
            patchWorldRequest.getSettings().setConsume(true);
            patchWorldRequest.getSettings().setPvp(true);
            patchWorldRequest.getSettings().setFire(true);
            patchWorldRequest.getSettings().setAutoUnload(false);
            patchWorldRequest.getSettings().setAutoBorder(false);
            patchWorldRequest.getSettings().setAutoTrimming(false);
            patchWorldRequest.getSettings().setKeepLocallyForDays(1);
            patchWorldRequest.getSettings().setMobLimit(2);
            connector.updateWorld(obj1Id, patchWorldRequest);
            changed1 = connector.getWorld(obj1Id);
            assertThat(changed1.getName(), is("changed"));
            assertThat(changed1.getSettings().getAccess(), is(AlqWorldAccess.Private));
            assertThat(changed1.getSettings().getChat(), is(AlqWorldChat.Private));
            assertThat(changed1.getSettings().getPrivateChatGroup(), is(obj1ChatGroupId));
            assertThat(changed1.getSettings().getGamemode(), is(AlqWorldGamemode.Survival));
            assertThat(changed1.getSettings().getSpawn().getSpawnX(), is(1.d));
            assertThat(changed1.getSettings().getSpawn().getSpawnY(), is(2.d));
            assertThat(changed1.getSettings().getSpawn().getSpawnZ(), is(3.d));
            assertThat(changed1.getSettings().getSpawn().getSpawnPitch(), is(4.d));
            assertThat(changed1.getSettings().getSpawn().getSpawnYaw(), is(5.d));
            assertThat(changed1.getSettings().isFindSafeSpawnLocation(), is(false));
            assertThat(changed1.getSettings().getTimeLock(), is(AlqWorldTimeLock.Night));
            assertThat(changed1.getSettings().getWeather(), is(AlqWorldWeather.Rain));
            assertThat(changed1.getSettings().isPhysics(), is(true));
            assertThat(changed1.getSettings().isMobs(), is(true));
            assertThat(changed1.getSettings().isInteraction(), is(false));
            assertThat(changed1.getSettings().getResourcepack(), is("respack"));
            assertThat(changed1.getSettings().getWelcomeMessage(), is("welcome"));
            assertThat(changed1.getSettings().getFarewellMessage(), is("farewell"));
            assertThat(changed1.getSettings().getDifficulty(), is(AlqWorldDifficulty.Hard));
            assertThat(changed1.getSettings().isDrops(), is(true));
            assertThat(changed1.getSettings().getProjectiles(), is(AlqWorldProjectiles.Allowed));
            assertThat(changed1.getSettings().isExplosions(), is(true));
            assertThat(changed1.getSettings().isHealthRegen(), is(false));
            assertThat(changed1.getSettings().isKeepInventory(), is(false));
            assertThat(changed1.getSettings().getSurvivalInventoryGroup(), is(obj1InventoryGroupId));
            assertThat(changed1.getSettings().isConsume(), is(true));
            assertThat(changed1.getSettings().isPvp(), is(true));
            assertThat(changed1.getSettings().isFire(), is(true));
            assertThat(changed1.getSettings().isAutoUnload(), is(false));
            assertThat(changed1.getSettings().isAutoBorder(), is(false));
            assertThat(changed1.getSettings().isAutoTrimming(), is(false));
            assertThat(changed1.getSettings().getKeepLocallyForDays(), is(1));
            assertThat(changed1.getSettings().getMobLimit(), is(2));

            cleanup.run();
            Assertions.assertThrows(AlqNotFoundResponseException.class, () -> {
                connector.getWorld(obj1Id);
            });

        } finally {
            cleanup.run();
        }
    }

    private void assertDefaultWorldSettings(AlqWorldSettings aws) {
        assertThat(aws.getAccess(), is(AlqWorldAccess.Public));
        assertThat(aws.getChat(), is(AlqWorldChat.Public));
        assertThat(aws.getPrivateChatGroup(), is(nullValue()));
        assertThat(aws.getGamemode(), is(AlqWorldGamemode.Creative));
        assertThat(aws.getSpawn(), is(nullValue()));
        assertThat(aws.isFindSafeSpawnLocation(), is(true));
        assertThat(aws.getTimeLock(), is(AlqWorldTimeLock.Day));
        assertThat(aws.getWeather(), is(AlqWorldWeather.Sun));
        assertThat(aws.isPhysics(), is(false));
        assertThat(aws.isMobs(), is(false));
        assertThat(aws.isInteraction(), is(true));
        assertThat(aws.getResourcepack(), is(nullValue()));
        assertThat(aws.getWelcomeMessage(), is(nullValue()));
        assertThat(aws.getFarewellMessage(), is(nullValue()));
        assertThat(aws.getDifficulty(), is(AlqWorldDifficulty.Normal));
        assertThat(aws.isDrops(), is(false));
        assertThat(aws.getProjectiles(), is(AlqWorldProjectiles.NotAllowed));
        assertThat(aws.isExplosions(), is(false));
        assertThat(aws.isHealthRegen(), is(true));
        assertThat(aws.isKeepInventory(), is(true));
        assertThat(aws.getSurvivalInventoryGroup(), is(nullValue()));
        assertThat(aws.isConsume(), is(false));
        assertThat(aws.isPvp(), is(false));
        assertThat(aws.isFire(), is(false));
        assertThat(aws.isAutoUnload(), is(true));
        assertThat(aws.isAutoBorder(), is(true));
        assertThat(aws.isAutoTrimming(), is(true));
        assertThat(aws.getKeepLocallyForDays(), is(nullValue()));
        assertThat(aws.getMobLimit(), is(0));
    }

    @Test
    public void deleteShouldBeIdempotent() {
        connector.deleteWorld(UUID.fromString("00000000-cd55-4ad6-972a-66f59ac3054c"));
    }

}
