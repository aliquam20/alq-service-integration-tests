package org.aliquam.integration.service.worlds;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.integration.AlqStubber;
import org.aliquam.perms.api.connector.AlqPermsConnector;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.players.api.connector.AlqPlayersConnector;
import org.aliquam.players.api.model.AlqPlayer;
import org.aliquam.players.api.model.AlqPlayerIdentifiers;
import org.aliquam.worlds.api.connector.AlqWorldsConnector;
import org.aliquam.worlds.api.connector.AlqWorldsFileConnector;
import org.aliquam.worlds.api.connector.AlqWorldsMemberConnector;
import org.aliquam.worlds.api.model.AlqWorld;
import org.aliquam.worlds.api.model.AlqWorldEnvironment;
import org.aliquam.worlds.api.model.AlqWorldFileStatus;
import org.aliquam.worlds.api.model.AlqWorldGenerator;
import org.aliquam.worlds.api.model.AlqWorldMember;
import org.aliquam.worlds.api.model.AlqWorldMemberType;
import org.aliquam.worlds.api.model.AlqWorldType;
import org.aliquam.worlds.api.model.GenerateAlqWorldJob;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

@Slf4j
public class AlqWorldsMembersTest {

    private final AlqPermsConnector permsConnector = new AlqPermsConnector(AlqIntegration.sessionManager);
    private final AlqPlayersConnector playersConnector = new AlqPlayersConnector(AlqIntegration.sessionManager);
    private final AlqWorldsConnector worldsConnector = new AlqWorldsConnector(AlqIntegration.sessionManager);
    private final AlqWorldsMemberConnector memberConnector = new AlqWorldsMemberConnector(AlqIntegration.sessionManager);

    @Test
    public void basicCrudShouldWork() throws IOException {
        UUID worldId = UUID.fromString("00000001-798f-46d5-94e4-bcaf03e6ed1f");
        UUID player1Id = UUID.fromString("00000010-798f-46d5-94e4-bcaf03e6ed1f");
        UUID player2Id = UUID.fromString("00000020-798f-46d5-94e4-bcaf03e6ed1f");

        Runnable cleanup = () -> {
            worldsConnector.deleteWorld(worldId);
            playersConnector.deletePlayer(player1Id);
            playersConnector.deletePlayer(player2Id);
            permsConnector.deleteGroup("alqWorldWhitelisted");
            permsConnector.deleteGroup("alqWorldAdmin");
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            permsConnector.putGroup(new AlqPermGroup("alqWorldWhitelisted", "whitelisted", 0));
            permsConnector.putGroup(new AlqPermGroup("alqWorldAdmin", "admin", 0));
            playersConnector.createPlayer(AlqStubber.getPlayer(player1Id, "p1"));
            playersConnector.createPlayer(AlqStubber.getPlayer(player2Id, "p2"));
            playersConnector.patchMinecraftIdentifiers(player1Id, player1Id);
            playersConnector.patchMinecraftIdentifiers(player2Id, player2Id);
            AlqWorld world = worldsConnector.createWorld(worldId, UUID.randomUUID());

            List<AlqWorldMember> members = memberConnector.listMembers(world);
            assertThat(members, is(empty()));

            memberConnector.addMember(world, player1Id, AlqWorldMemberType.Whitelisted);
            memberConnector.addMember(world, player2Id, AlqWorldMemberType.Admin);

            members = memberConnector.listMembers(world);
            assertThat(members, contains(
                    new AlqWorldMember(worldId, player1Id, AlqWorldMemberType.Whitelisted),
                    new AlqWorldMember(worldId, player2Id, AlqWorldMemberType.Admin)
            ));

            memberConnector.delMember(world, player1Id);
            memberConnector.delMember(world, player2Id);

            members = memberConnector.listMembers(world);
            assertThat(members, is(empty()));

        } finally {
            cleanup.run();
        }
    }

}
