package org.aliquam.integration.service.worlds;

import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.worlds.api.connector.AlqWorldsConnector;
import org.aliquam.worlds.api.connector.AlqWorldsFileConnector;
import org.aliquam.worlds.api.model.AlqWorld;
import org.aliquam.worlds.api.model.AlqWorldEnvironment;
import org.aliquam.worlds.api.model.AlqWorldFileStatus;
import org.aliquam.worlds.api.model.AlqWorldGenerator;
import org.aliquam.worlds.api.model.AlqWorldType;
import org.aliquam.worlds.api.model.GenerateAlqWorldJob;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.Callable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

@Slf4j
public class AlqWorldsGeneratorTest {

    private final AlqWorldsConnector worldsConnector = new AlqWorldsConnector(AlqIntegration.sessionManager);
    private final AlqWorldsFileConnector fileConnector = new AlqWorldsFileConnector(AlqIntegration.sessionManager);

    @Test
    public void generateWorldFileWorks() throws Exception {
        UUID worldId = UUID.fromString("00000001-8439-4aec-b3f1-5426131be154");

        Runnable cleanup = () -> {
            worldsConnector.deleteWorld(worldId);
        };
        try(NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            AlqWorld world = worldsConnector.createWorld(worldId, UUID.randomUUID());
            assertThat(world.getFile(), is(nullValue()));

            AlqWorldGenerator generatorSettings = AlqWorldGenerator.builder()
                    .generator(null)
                    .type(AlqWorldType.FLAT)
                    .environment(AlqWorldEnvironment.NORMAL)
                    .seed(2137L)
                    .rawGeneratorSettings("")
                    .generateStructures(false)
                    .hardcore(false)
                    .build();
            GenerateAlqWorldJob generateJob = GenerateAlqWorldJob.builder()
                    .world(worldId)
                    .generator(generatorSettings)
                    .build();
            fileConnector.generateWorldFile(generateJob, true);

            RetryConfig retryConfig = RetryConfig.custom()
                    .maxAttempts(40)
                    .intervalFunction(numAttempts -> 5000L)
                    .retryOnResult(Objects::isNull)
                    .build();
            Retry retry = Retry.of("Wait for world generation", retryConfig);

            retry.getEventPublisher()
                    .onRetry(event -> log.info("World not generated yet. Waiting for retry-check in {}; lastThrowable: {}",
                            event.getWaitInterval(), event.getLastThrowable()));

            Callable<Boolean> decoratedCall = Retry.decorateCallable(retry, () -> {
                AlqWorld w = worldsConnector.getWorld(worldId);
                log.info("world file status: {}", w.getFile().getStatus());
                if(w.getFile().getStatus() == AlqWorldFileStatus.COMMITTED) {
                    return true;
                } else {
                    return null;
                }
            });
            Boolean result = decoratedCall.call();
            assertThat(result, is(true));

            InputStream worldStream = fileConnector.getWorldFile(worldId);
            byte[] worldBytes = worldStream.readAllBytes();
            assertThat(worldBytes.length, greaterThan(100));
        } finally {
            cleanup.run();
        }
    }

}
