package org.aliquam.integration.service.worlds;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.worlds.api.connector.AlqWorldsConnector;
import org.aliquam.worlds.api.connector.AlqWorldsFileConnector;
import org.aliquam.worlds.api.model.AlqWorld;
import org.aliquam.worlds.api.model.AlqWorldEnvironment;
import org.aliquam.worlds.api.model.AlqWorldFileStatus;
import org.aliquam.worlds.api.model.AlqWorldGenerator;
import org.aliquam.worlds.api.model.AlqWorldType;
import org.aliquam.worlds.api.model.GenerateAlqWorldJob;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

@Slf4j
public class AlqWorldsFileTest {

    private final AlqWorldsConnector worldsConnector = new AlqWorldsConnector(AlqIntegration.sessionManager);
    private final AlqWorldsFileConnector fileConnector = new AlqWorldsFileConnector(AlqIntegration.sessionManager);

    @Test
    public void basicCrudShouldWork() throws IOException {
        UUID worldId = UUID.fromString("00000001-ba2c-477d-99ca-252e26ea67d7");

        Runnable cleanup = () -> {
            worldsConnector.deleteWorld(worldId);
        };
        try (NkTrace ignored = NkTrace.info(log)) {
            cleanup.run();

            AlqWorld world = worldsConnector.createWorld(worldId, UUID.randomUUID());
            assertThat(world.getFile(), is(nullValue()));

//            Supplier<InputStream> helloInputSupplier = () -> new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8));
            AlqWorldGenerator generatorSettings = AlqWorldGenerator.builder()
                    .generator("generator")
                    .rawGeneratorSettings("rawGenSettings")
                    .generateStructures(true)
                    .environment(AlqWorldEnvironment.CUSTOM)
                    .type(AlqWorldType.AMPLIFIED)
                    .hardcore(true)
                    .seed(123456L)
                    .build();
            GenerateAlqWorldJob generateJob = GenerateAlqWorldJob.builder()
                    .world(worldId)
                    .generator(generatorSettings)
                    .build();
            fileConnector.generateWorldFile(generateJob, false);

            world = worldsConnector.getWorld(worldId);
            assertThat(world.getFile().getStatus(), is(AlqWorldFileStatus.QUEUED));
            assertThat(world.getFile().getGenerator(), is(generatorSettings));

            fileConnector.commitGeneratedWorldFile(worldId, Path.of("test_input", "hello.txt"));
            world = worldsConnector.getWorld(worldId);
            assertThat(world.getFile().getStatus(), is(AlqWorldFileStatus.COMMITTED));

            InputStream helloFileStream = fileConnector.getWorldFile(worldId);
            String helloFileString = new String(helloFileStream.readAllBytes(), StandardCharsets.UTF_8);
            assertThat(helloFileString, is("hello"));

//            Supplier<InputStream> worldInputSupplier = () -> new ByteArrayInputStream("world".getBytes(StandardCharsets.UTF_8));
            fileConnector.updateWorldFile(worldId, Path.of("test_input", "world.txt"), false);

            InputStream worldFileStream = fileConnector.getWorldFile(worldId);
            String worldFileString = new String(worldFileStream.readAllBytes(), StandardCharsets.UTF_8);
            assertThat(worldFileString, is("world"));

            fileConnector.deleteWorldFile(worldId);

            world = worldsConnector.getWorld(worldId);
            assertThat(world.getFile(), is(nullValue()));

        } finally {
            cleanup.run();
        }
    }

    @Test
    public void deleteShouldBeIdempotent() {
        fileConnector.deleteWorldFile(UUID.fromString("00000000-0146-482e-9ff5-450f0ae5da47"));
    }

}
