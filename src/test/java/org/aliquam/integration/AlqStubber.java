package org.aliquam.integration;

import lombok.extern.slf4j.Slf4j;
import net.kawinski.logging.NkTrace;
import org.aliquam.cum.AlqJson;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.integration.AlqIntegration;
import org.aliquam.players.api.connector.AlqPlayersConnector;
import org.aliquam.players.api.model.AlqPlayer;
import org.aliquam.players.api.model.AlqPlayerIdentifiers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.TimeZone;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Slf4j
public class AlqStubber {

    public static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2020, 10, 10, 12, 30, 30);

    public static AlqPlayer getPlayer(UUID id, String name) {
        return new AlqPlayer(id, LOCAL_DATE_TIME, name,
                UUID.randomUUID(), UUID.randomUUID(), 0d, TimeZone.getDefault(), "EN");
    }

}
