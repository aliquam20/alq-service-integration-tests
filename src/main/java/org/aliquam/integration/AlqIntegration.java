package org.aliquam.integration;

import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.connector.AlqSessionManager;

public class AlqIntegration {
    public static AlqSessionManager sessionManager = AlqSessionApi.provideAlqSessionManager();

    public String getHello() {
        return "hello";
    }

}
